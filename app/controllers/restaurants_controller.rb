class RestaurantsController < ApplicationController
  def index
  	@restaurants = Restaurant.all
  end

  def show
  	@restaurant = Restaurant.find(params[:id])
  end

  def new
  	@restaurant = Restaurant.new
    @locations = Location.all
  end

  def create
  	authorize! :index, @user, :message => 'Not authorized as an administrator.'
  	@restaurant = Restaurant.new(params[:restaurant])
  	if @restaurant.save
  	  redirect_to @restaurant, :notice => "Restaurant added"
  	else
  	  render :new
  	end
  	
  end

  def edit
  	@restaurant = Restaurant.find(params[:id])
    @locations = Location.all
  	
  end
  def update
  	authorize! :index, @user, :message => 'Not authorized as an administrator.'
  	@restaurant = Restaurant.find(params[:id])
    if @restaurant.update_attributes(params[:restaurant])
      # Handle a successful update.
      redirect_to @restaurant, :notice => "Restaurant successfully updated"
    else
      render 'edit'
    end
  end
  def destroy
  	authorize! :index, @user, :message => 'Not authorized as an administrator.'
  	@restaurant = Restaurant.find(params[:id])
  	@restaurant.destroy
    redirect_to restaurants_path, :notice => "Restaurant deleted."

  	
  end
end
