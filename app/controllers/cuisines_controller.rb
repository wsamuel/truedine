class CuisinesController < ApplicationController
  def index
  	@cuisines = Cuisine.all
  end

  def show
  	@cuisine = Cuisine.find(params[:id])
  end

  def new
  	@cuisine =Cuisine.new
  end

  def create
  	authorize! :index, @user, :message => 'Not authorized as an administrator.'
  	@cuisine = Cuisine.new(params[:cuisine])
  	if @cuisine.save
  	  redirect_to @cuisine, :notice => "Cuisine added"
  	else
  	  render :new
  	end
  	
  end
end
