class ReservationsController < ApplicationController

  before_filter :load_restaurant
  def index
    
    @reservations = @restaurant.reservations.all
    @reservations_by_date = @reservations.group_by(&:day)
    @date = params[:date] ? Date.parse(params[:date]) : Date.today
  end

  def show
  	@reservation = @restaurant.reservations.find(params[:id])
  end

  def new
    @reservation = @restaurant.reservations.new
  end

  def edit
  	@reservation = @restaurant.reservations.find(params[:id])
  end

  def create
  	@reservation = @restaurant.reservations.new(params[:reservation])
  	if @reservation.save
  	  redirect_to [@restaurant,@reservation], :notice => "Your Reservation is complete"
  	else
  	  render :new
  	end
  	
  end

  private
    def load_restaurant
      @restaurant = Restaurant.find(params[:restaurant_id])
      
    end
end
