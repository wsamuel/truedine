class AreasController < ApplicationController
  def index
  	@areas= Area.all
  end

  def new
  	@area = Area.new
  end

  def edit
  	@area =Area.find(params[:id])
  end

  def show
  	@area = Area.find(params[:id])
  end

  def create
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
  	@area = Area.new(params[:area])
  	if @area.save
  	  redirect_to @area, :notice => "Area added"
  	else
  	  render :new
  	end
  	
  end

  def update
  	authorize! :index, @user, :message => 'Not authorized as an administrator.'
  	@area = Area.find(params[:id])
    if @area.update_attributes(params[:area])
      # Handle a successful update.
      redirect_to @area, :notice => "Area successfully updated"
    else
      render 'edit'
    end
  end

  def destroy
  	authorize! :index, @user, :message => 'Not authorized as an administrator.'
  	@area = Area.find(params[:id])
  	@area.destroy
    redirect_to areas_path, :notice => "Restaurant deleted."
  end


end
