class LocationsController < ApplicationController
  def index
  	@locations = Location.all
  end

  def new
  	@location = Location.new
  end

  def edit
  	@location = Location.find(params[:id])
  end

  def show
    @location = Location.find(params[:id])
  end

  def create
  	authorize! :index, @user, :message => 'Not authorized as an administrator.'
  	@location = Location.new(params[:location])
  	if @location.save
  	  redirect_to @location, :notice => "Location added"
  	else
  	  render :new
  	end
  	
  end

  def update
  	authorize! :index, @user, :message => 'Not authorized as an administrator.'
  	@location = Location.find(params[:id])
    if @location.update_attributes(params[:location])
      # Handle a successful update.
      redirect_to @location, :notice => "Location successfully updated"
    else
      render 'edit'
    end
  end

  def destroy
  	authorize! :index, @user, :message => 'Not authorized as an administrator.'
  	@location = Location.find(params[:id])
  	@location.destroy
    redirect_to locations_path, :notice => "Location deleted."

  	
  end
end
