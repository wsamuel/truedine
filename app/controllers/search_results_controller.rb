class SearchResultsController < ApplicationController
  def index
  	@restaurants = Restaurant.text_search(params[:query]).page(params[:page]).per_page(3)
    
  end
end
