class Cuisine < ActiveRecord::Base
  attr_accessible :cuisine_name
  has_and_belongs_to_many :restaurants
end
