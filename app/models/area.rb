class Area < ActiveRecord::Base
  attr_accessible :area_name, :location_id
  belongs_to :location
  has_many :restaurants
end
