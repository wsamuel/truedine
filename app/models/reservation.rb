class Reservation < ActiveRecord::Base
  attr_accessible :day, :number_of_people, :special_requirements, :time, :restaurant_id
  belongs_to :restaurant
end
