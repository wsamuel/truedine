class Location < ActiveRecord::Base
  attr_accessible :location_name
  has_many :areas
  has_many :restaurants, through: :areas
  validates_uniqueness_of :location_name, :case_sensitive => :false
end
