class Restaurant < ActiveRecord::Base
  attr_accessible  :description, :menu, :restaurant_name, :area_id
  attr_accessible :cuisine_ids
  attr_accessible :filepicker_url
  belongs_to :area
  has_and_belongs_to_many :cuisines
  has_many :reservations

  include PgSearch
  pg_search_scope :search, against: [:restaurant_name, :description],
    using: {tsearch: {dictionary: "english"} },
    associated_against: {cuisines: :cuisine_name}

  def self.text_search(query)
  	if query.present?
      search(query)
  	  #where("restaurant_name @@ :q or description @@ :q", q: query)
  		
  	else
  	  scoped	
  	end
  	
  end
end
