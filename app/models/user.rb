class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :role_ids, :as => :admin
  attr_accessible :first_name, :last_name, :email, :mobile, :password, :password_confirmation, :remember_me
  before_create :assign_role
  # attr_accessible :title, :body
  validates_presence_of :first_name, :last_name
  validates_presence_of :mobile, length: {minimum: 10}

  def assign_role
   # assign a default role if no role is assigned
    self.add_role :user if self.roles.first.nil?
  end
end
