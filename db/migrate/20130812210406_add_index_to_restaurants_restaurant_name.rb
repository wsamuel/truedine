class AddIndexToRestaurantsRestaurantName < ActiveRecord::Migration
  def change
    add_index :restaurants, :restaurant_name
  end
end
