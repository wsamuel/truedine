class RemoveColumnNameFromRestaurants < ActiveRecord::Migration
  def up
    remove_column :restaurants, :name
  end

  def down
    add_column :restaurants, :name, :string
  end
end
