class AddAreaIdToRestaurants < ActiveRecord::Migration
  def change
    add_column :restaurants, :area_id, :integer
  end
end
