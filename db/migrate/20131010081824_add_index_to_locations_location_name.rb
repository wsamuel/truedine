class AddIndexToLocationsLocationName < ActiveRecord::Migration
  def change
  	add_index :locations, :location_name, unique: true
  end
end
