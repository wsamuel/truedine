class AddCuisineNameToCuisines < ActiveRecord::Migration
  def change
    add_column :cuisines, :cuisine_name, :string, unique: true
  end
end
