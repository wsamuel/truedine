class AddRestaurantNameToRestaurants < ActiveRecord::Migration
  def change
    add_column :restaurants, :restaurant_name, :string
  end
end
