class RemoveColumnNameFromCuisines < ActiveRecord::Migration
  def up
    remove_column :cuisines, :name
  end

  def down
    add_column :cuisines, :name, :string
  end
end
