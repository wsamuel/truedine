class RemoveColumnRestaurantIdFromCuisines < ActiveRecord::Migration
  def up
    remove_column :cuisines, :restaurant_id
  end

  def down
    add_column :cuisines, :restaurant_id, :integer
  end
end
