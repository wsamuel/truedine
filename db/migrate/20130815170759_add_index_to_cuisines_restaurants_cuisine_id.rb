class AddIndexToCuisinesRestaurantsCuisineId < ActiveRecord::Migration
  def change
  	add_index :cuisines_restaurants, :cuisine_id
  end
end
