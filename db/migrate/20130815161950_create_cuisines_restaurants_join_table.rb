class CreateCuisinesRestaurantsJoinTable < ActiveRecord::Migration
  def change
    create_table :cuisines_restaurants, id: false do |t|
      t.integer :restaurant_id
      t.integer :cuisine_id
    end
  end
end
