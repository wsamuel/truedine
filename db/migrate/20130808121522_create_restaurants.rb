class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :name
      t.text :description
      t.text :menu
      t.string :cuisine

      t.timestamps
    end
  end
end
