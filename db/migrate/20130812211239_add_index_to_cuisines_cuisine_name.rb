class AddIndexToCuisinesCuisineName < ActiveRecord::Migration
  def change
  	add_index :cuisines, :cuisine_name
  end
end
