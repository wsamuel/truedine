class AddNameOfAttrForFilePickerUrlToRestaurant < ActiveRecord::Migration
  def up
    add_column :restaurants, :filepicker_url, :string
  end

  def down
    remove_column :restaurants, :filepicker_url
  end
end
