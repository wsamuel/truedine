class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.date :day
      t.string :time
      t.integer :number_of_people
      t.text :special_requirements

      t.timestamps
    end
  end
end
