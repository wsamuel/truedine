# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131011120126) do

  create_table "areas", :force => true do |t|
    t.string   "area_name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "location_id"
  end

  create_table "cuisines", :force => true do |t|
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "cuisine_name"
  end

  add_index "cuisines", ["cuisine_name"], :name => "index_cuisines_on_cuisine_name"

  create_table "cuisines_restaurants", :id => false, :force => true do |t|
    t.integer "restaurant_id"
    t.integer "cuisine_id"
  end

  add_index "cuisines_restaurants", ["cuisine_id"], :name => "index_cuisines_restaurants_on_cuisine_id"
  add_index "cuisines_restaurants", ["restaurant_id"], :name => "index_cuisines_restaurants_on_restaurant_id"

  create_table "locations", :force => true do |t|
    t.string   "location_name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "locations", ["location_name"], :name => "index_locations_on_location_name", :unique => true

  create_table "reservations", :force => true do |t|
    t.date     "day"
    t.string   "time"
    t.integer  "number_of_people"
    t.text     "special_requirements"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "restaurant_id"
  end

  create_table "restaurants", :force => true do |t|
    t.text     "description"
    t.text     "menu"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "restaurant_name"
    t.integer  "area_id"
    t.string   "filepicker_url"
  end

  add_index "restaurants", ["restaurant_name"], :name => "index_restaurants_on_restaurant_name"

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "mobile"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

end
